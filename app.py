import tensorflow as tf
from tensorflow import keras
from keras import models
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
import numpy as np
import sys
import os
import pathlib
from jinja2 import Template, Environment, FileSystemLoader

# Načítání a formátování obrázků
def load_image(filename):
	img = load_img(filename, target_size=(32, 32))
	img = img_to_array(img)
	img = img.reshape(1, 32, 32, 3)
	img = img.astype('float32')
	img = img / 255.0
	return img


if(sys.argv[1]):
    # Příprava cest, načtení modelu
    path = pathlib.Path().absolute()
    os.chdir(path)
    path = path / "neuronka"
    neuronka = keras.models.load_model(path)
    folder_path = sys.argv[1]

    airplanes = []
    automobiles = []
    birds = []
    cats = []
    deers = []
    dogs = []
    frogs = []
    horses = []
    ships = []
    trucks = []

    # Iterace nad obrázky ve složce
    for fname in os.listdir(folder_path):
        img_path = os.path.join(folder_path, fname)
        img = load_image(img_path)

        # Vyhodnocení za pomoci modelu
        result = neuronka.predict(img)
        result = np.argmax(result)

        # Zařazení do kategorie
        if result == 0:
            airplanes.append(img_path)
        elif result == 1:
            automobiles.append(img_path)
        elif result == 2:
            birds.append(img_path)
        elif result == 3:
            cats.append(img_path)
        elif result == 4:
            deers.append(img_path)
        elif result == 5:
            dogs.append(img_path)
        elif result == 6:
            frogs.append(img_path)
        elif result == 7:
            horses.append(img_path)
        elif result == 8:
            ships.append(img_path)
        elif result == 9:
            trucks.append(img_path)

    # Načtení Jinja templatu pro tvorbu HTML
    root = os.path.dirname(os.path.abspath(__file__))
    templates_dir = os.path.join(root, 'templates')
    env = Environment( loader = FileSystemLoader(templates_dir) )
    template = env.get_template('index.html')
 
    # Tvorba výsledného HTML
    filename = os.path.join(root, 'html', 'index.html')
    with open(filename, 'w') as fhtml:
        fhtml.write(template.render(
            airplanes = airplanes,
            automobiles = automobiles,
            birds = birds,
            cats = cats,
            deers = deers,
            dogs = dogs,
            frogs = frogs,
            horses = horses,
            ships = ships,
            trucks = trucks
        ))
else:
    print("Nebyl zadán adresář s obrázky ke klasifikaci.")
