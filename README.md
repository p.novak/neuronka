# Projekt Neuronka - Maturitní práce
## Petr Novák 
### Gymnázium Nad Alejí 1952, Praha 6
#### 2020/21
___
### Zadání

#### *Obsah práce:*

Naprogramujte systém založený na neuronové síti, který bude schopný automaticky roztřídit zadanou
sérii obrázků podle podobnosti (kategorie).

#### *Funkce:*

Systém bude fungovat jako aplikace na příkazové řádce. V následujícím textu volně zaměňujeme
roztřídění podobných fotografií (tj. je na nich podobný objekt) a roztřídění podle kategorií (tj. jsou
podobné vůči dané množině kategorií), protože z uživatelského hlediska jde o podobné operace a rozdíl je
silně předurčen výchozím datasetem.

#### *Třídění fotografií:*

Výchozí funkcí bude roztřídení fotografií podle podobnosti. Systém využije vestavěnou neuronovou síť,
která bude natrénována z předem vybraných fotografií.

#### *Trénování neuronové sítě:*

Uživatel bude moci specifikovat též sada obrázků jako svojí tréningovou sada (např. uložené v
samostatných podsložkách). Program se pak naučí třídit pomocí takto zadaného vzoru.

#### *Zobrazení výsledků:*

Výstupem programu bude jednoduchá HTML galerie, kde pro každou kategorii bude existovat
samostatná stránka s náhledy na jednotlivé fotografii.

#### *Dataset:*

Součástí odevzdávané práce bude i použitý dataset pro výchozí neuronovou síť.

___

### Řešení a osobní poznámky k němu

Jako framework pro tvorbu neuronové sítě jsem zvolil Keras spadající pod TensorFlow.

Ze začátku jsem se pokoušel sestavit na základě vlastního datasetu SNN (Siamese Neural Network), která je výhodná díky dynamickému postoji ke třídám, jelikož nepotřebuje mít předem definovány všechny třídy. SNN by měřila vzdálenost mezi vektorovými reprezentacemi obrázků a následně je podle ní seskupila. Dataset jsem vytvořil webscrappingem skrze FlickAPI. SNN jsem však musel zavrhnout kvůli hardwarovým limitacím a nedostatkům jež nesl můj nedostačující dataset. V době kdy jsem tak neradostně učinil dosahovala neuronová síť přesnosti/úspěšnosti přibližně 14,5%.

Začal jsem tedy sestavovat síť typu CNN (Convutional Neural Network), která je omezená na určování předem definovaných/naučených tříd. Předělal jsem taktéž dataset, nicméně CNN k dosažení použitelné přesnosti mnohem většího objemu dat než SNN. Přidal jsem tedy augmentaci dat, ale i tak byly problémy způsobené optimalizací, rozsahem a dalšími limitacemi datasetu dle mého názoru příliš velké.

Začal jsem tedy používat CIFAR10 dataset, který je připraven k použití v rámci frameworku Keras. Obsahuje 60000 obrázků v RGB s rozlišením 32x32, což z něj dělá pro účely této práce ideální volbu. Deklarace modelu je nicméně dostatečně flexibilní, aby bylo možné dataset zaměnit za jiný a znova natrénovat model.

Výsledek jsem se rozhodl vypisovat do jedné stránky a nikoliv do jednotlivých stránek pro kategorie, pro dle mého názoru větší přehlednost a estetičtější dojem. Zároveň jsem se tak mohl soustředit více na model samotný než na generování html stránek.

Za použití zmíněného datasetu se mi podařilo dosáhnout přesnosti modelu cca 81%. 

Poděkování patří Michalu Šteflovi za zapůjčení dostatečně výkoného hardwaru pro samotný trénink modelu.

___

### Použití

#### *Funkcionalita:*

Program roztřídí obrázky do deseti kategorií a to:

1. letadla
2. auta
3. ptáci
4. kočky
5. jeleni
6. psy
7. žáby
8. koně
9. lodě
10. kamiony

#### *Požadavky:*

Ke spuštění programu je třeba mít nainstalovaný *Python* a *Pip*, který by měl být součástí instalačního balíčku.

Pro instalaci potřebných balíčků je pak nutné zpustit příkaz `pip install -r requirements.txt`.

Pokud pip vyhodí chybou hlášku a nenainstaluje některý z balíčku, nainstalujte je manuálně, příkazem `pip install <package>` kde `<package>`je balíček, novější verze by neměla program rozbít.

Chcete-li upravovat `model.ipynb` musíte nainstalovat Jupyter.

#### *Spuštění programu:*

Program se spustí příkazem `python app.py <directory_path>`.

`<directory_path>` je absolutní cesta k adresáři s obrázky ve formátu JPEG k roztřízení. Například `D:\Obrazky\`

#### *Výsledek:*

Výsledný html soubor se nachází ve složce html, kde jej naleznete pod označením `index.html`.

#### *Vlastní dataset:*

Pokud chcete použít vlastní dataset, je třeba upravit parametry `image_size` a `class_names` tak aby odpovídaly novému datasetu. 

Zvažte také úpravu `batch_size` a `buffer_size` pro dosažení optimálmích výsledků při novém učení. 

Místo datasetu CIFAR10 pak načtěte váš dataset, tak aby odpovídal *NumPy arrays* `x_train, y_train` a `x_test, y_test`. 

Smažte složku */neuronka* a spusťte `model.ipnb` pomocí rozhraní Jupyter.

Kategorie v `app.py` nahraďte takovými co odpovídají datasetu a obdobně upravte `index.html` ve složce */templates*